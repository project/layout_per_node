# Layout Compatibility

[Layout Per Node](https://drupal.org/project/layout_per_node) is designed to be
compatible with any layout defined via
[Layout Discovery](https://www.drupal.org/node/2296423). For example,
the [Bootstrap Layouts](https://www.drupal.org/project/bootstrap_layouts)
module provides a number of layouts with classes to work with the
[Bootstrap](http://getbootstrap.com/) theme framework. So long as a layout
follows CSS naming patterns of the core-provided Layout Discovery module,
the Layout Per Node will be able to find its regions & work properly.
Here is an overview of what is required:

## 1. Include a `region--<regionname>` class

Below is an excerpt from the ["layout twocol_bricks"](https://github.com/drupal/drupal/blob/8.4.x/core/modules/layout_discovery/layouts/twocol_bricks/layout--twocol-bricks.html.twig) layout provided by core (8.4.x):

```twig
{% if content %}
  <div{{ attributes.addClass(classes) }}>
    {% if content.top %}
      <div {{ region_attributes.top.addClass('layout__region', 'layout__region--top') }}>
        {{ content.top }}
      </div>
    {% endif %}

    {% if content.first_above %}
      <div {{ region_attributes.first_above.addClass('layout__region', 'layout__region--first-above') }}>
        {{ content.first_above }}
      </div>
    {% endif %}
```
Note that each region's content is wrapped in a `<div>` which includes
a `layout__region--<regionname>` class. The "top" region would render as this:
```
<div class="layout__region layout__region--top">
```
Layout Per Node needs the region name so that it can know where to to place
content, and to save content placement settings. If you plan to create a layout
and use it with Layout Per Node, make sure that each of your regions includes a
similar CSS declaration:
- The CSS declaration must begin with `region--`
- It must end with the machine name of the region
- Underscores must be converted to dashes (as shown in the above example for
the `first_above` region, which becomes `layout__region--first-above`)
- Your layout does **not** have to use `<div>` elements for the wrappers.

## 2. Allow attributes to be added to your node.html.twig
Below is an excerpt from the core Bartik theme's node.html.twig file:

```twig
{ attach_library('classy/node') }}
<article{{ attributes.addClass(classes) }}>
  <header>
    {{ title_prefix }}
```

The inclusion of `{{ attributes }}` in the `<article>` wrapper allows other
modules to inject attributes at the node level via preprocessing.
Layout Per Node injects a data attribute, `data-layout-per-node` here so that
it can "find" the node without relying on any assumptions about node-level
markup.

So long as you are using a theme that implements the same (best practice)
approach, Layout Per Node will work out of the box.

## 2b. ...Or add a data-attribute to your layout
If for whatever reason, you don't want to or can't control this part of the
site, you can add `data-layout-per-node=1` to your layouts' html.twig files.
For example, the above "layout__twocol" layout could add the following to
its wrapping `<div>`:

```twig
{% if content %}
  <div{{ attributes.addClass(classes) }} data-layout-per-node="1">
    {% if content.top %}
      <div {{ region_attributes.top.addClass('layout__region', 'layout__region--top') }}>
        {{ content.top }}
      </div>
    {% endif %}

    {% if content.first_above %}
      <div {{ region_attributes.first_above.addClass('layout__region', 'layout__region--first-above') }}>
        {{ content.first_above }}
      </div>
    {% endif %}
```
