<?php

namespace Drupal\Tests\layout_per_node\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests routes info pages and links.
 *
 * @group layout_per_node
 */
class BasicEntityTest extends BrowserTestBase {

  /**
   * Use the 'standard' installation for Drupal-provided node types.
   *
   * @var string
   */
  protected $profile = 'standard';

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'layout_per_node',
    'field_layout',
    'layout_discovery',
  ];

  /**
   * Tests entity saves all components of the entity on the DB.
   */
  public function testSaveEntity() {
    // Creating the layout blob.
    $layout = unserialize('a:1:{s:13:"layout_twocol";a:2:{s:5:"first";a:1:{s:4:"body";s:5:"field";}s:6:"second";a:1:{s:29:"views_block:who_s_new-block_1";s:12:"plugin_block";}}}');
    // Creating the data array that will be saved as the layout entity.
    $data = [
      'entity_id' => '8',
      'vid' => '1',
      'entity_type' => 'node',
      'layout' => $layout,
    ];
    // Creating the entity.
    $layout_entity = entity_create('layout_per_node_layout', [
      'entity_id' => $data['entity_id'],
      'vid' => $data['vid'],
      'entity_type' => $data['entity_type'],
      'layout' => $data['layout'],
    ]);
    // Saving entity.
    $layout_entity->save();
    // Loading previously saved entity where id = 1 since its the only row.
    $entity = entity_load('layout_per_node_layout', 1);
    // Asserting that the previously saved values match the original $data ones.
    $this->assertEquals($entity->id(), 1);
    $this->assertEquals($entity->get('entity_id')->getString(), $data['entity_id']);
    $this->assertEquals($entity->get('vid')->getString(), $data['vid']);
    $this->assertEquals($entity->get('entity_type')->getString(), $data['entity_type']);
    $this->assertArrayHasKey('uuid', $entity->toArray());
    $this->assertEquals($layout, $entity->get('layout')->getValue()[0]);
  }

}
