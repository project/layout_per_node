<?php

namespace Drupal\Tests\layout_per_node\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests routes info pages and links.
 *
 * @group layout_per_node
 */
class BasicConfigTest extends BrowserTestBase {

  /**
   * Use the 'standard' installation for Drupal-provided node types.
   *
   * @var string
   */
  protected $profile = 'standard';

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'layout_per_node',
    'field_layout',
    'layout_discovery',
  ];

  /**
   * Tests entity saves all components of the entity on the DB.
   */
  public function testSaveConfig() {

    // Creating allowed layouts array.
    $allowed = [
      'layout_onecol' => 'layout_onecol',
      'layout_twocol_bricks' => '0',
    ];
    // Saving and asserting a boolean value to the 'enabled' attribute.
    $config = \Drupal::config('layout_per_node.content_type.article');
    $config = \Drupal::service('config.factory')->getEditable('layout_per_node.content_type.article');
    $config->set('enabled', 1)->save();
    $this->assertEquals($config->get('enabled'), TRUE);
    // Saving and asserting  the $allowed array to the 'allowed' attribute.
    $config = \Drupal::service('config.factory')->getEditable('layout_per_node.content_type.article');
    $config->set('allowed', $allowed)->save();
    $this->assertEquals($config->get('allowed'), $allowed);
  }

}
