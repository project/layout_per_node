CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation & Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------
This module allows content builders to select between node layouts and to assign
reusable content (blocks) and page-specific content (fields) via a
drag-and-drop UI.

For a detailed overview, see INTRODUCTION.md or watch
https://youtu.be/E8f8oL7dZkQ

REQUIREMENTS
------------
- PHP: 5.5.9 or above
- Drupal core: 8.3.x or above
- This module has no dependencies on other contributed modules

INSTALLATION & CONFIGURATION
----------------------------
1. After enabling this module, go to any node content type's edit page
(e.g., `/admin/structure/types/manage/page`)
2. Click the "**Layout Per Node**" vertical tab & tick "Enable layout per node"
- Optionally, define which layouts, provided by the system, should be available
to choose from on this content type.
 - If no layouts are explicitly selected, all will be available.
4. Add a new node of that type and save data to any of its fields.
5. In the "view" mode of that node, you will see a new "**Layout Editor**" tab.
Clicking it will enable drag-and-drop layout editing.
 - The "**Switch Layouts**" tab allows selection of any available layouts.
 - The "**Save Layout**" tab will write staged changes to a new node revision
 - The "**Add Content**" button within each region will provide a pop-up for
 selecting any available fields or site-wide content (i.e., blocks)
6. Initially, layout_per_node makes available two categories of reusable
content: Views blocks and custom blocks (of any block type). To enable blocks
provided by other modules, visit `/admin/config/content/lpn`

TROUBLESHOOTING
---------------
- This module does not provide any layouts. It finds and makes available all
layouts registered through the core Layout Discovery system. For more
information, see Layout-Compatibility.md
- As part of the Drupal 8.4.x release, some regions were renamed in the layouts
provided by layout_discovery core (see: https://github.com/drupal/drupal/commit/5184e99d3f69f853fce0e539d1923d5494713857#diff-c3a60429cd3ece82ee2cadfe877ef95cR18).
If you were using layout_per_node prior to 8.4.x, nodes that use these core
layouts may appear to have lost their layout, since the regions no longer exist.
You will either need to resave the nodes with the new region data or write a
script that updates the region names in the layout_per_node table
programmatically.

FAQ
---
1. Why "Layout Per *Node*"?
Drupal 8 has made great strides in standardizing Drupal's disparate 'content
buckets' around the Entity API: in D8, taxonomies, blocks, and users share the
same API as node types -- and as such, they are also all "fieldable" and can
use `field_layout`. There is, therefore, no inherent *technical* limitation to
implementing Layout Per Node's drag-and-drop UI and layout storage backend to
taxonomy pages, blocks, users, and arbitrary content entities created by other
projects. The maintainers' decision to limit the ability of the drag-and-drop UI
to node types was therefore an attempt to best support the majority of use cases
while scoping the code implementation for maintainability.

MAINTAINERS
-----------
Current maintainers:
 * Francine Bray (brayfe) - https://www.drupal.org/u/brayfe
 * Tyler Fahey (twfahey) - https://www.drupal.org/u/twfahey
 * Mark Fullmer (mark_fullmer) - https://www.drupal.org/u/mark_fullmer
 * Paul Grotevant (gravelpot) - https://www.drupal.org/u/gravelpot

This project has been sponsored by:
* The University of Texas at Austin
